#!/usr/bin/python
import socket

STIMEOUT = 2
OPEN_HOSTS = {}
IP_RANGE = '10.208.200.'

def create_new_socket():
	return socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def destroy_socket(s_object):
	s_object.close()
	del s_object

def add_host_report(ip, banner):
	OPEN_HOSTS[ip] = banner.strip('\n')

def print_host_report():

	print "Found: ", len(OPEN_HOSTS), "hosts"
	for IP, BANNER in OPEN_HOSTS.iteritems():
		print IP, ":", BANNER


def connect_host(addr):
	HOST_IP = IP_RANGE + str(addr)

	try:
		socket.setdefaulttimeout(STIMEOUT)
		mys = create_new_socket()
		mys.connect( (HOST_IP, 22) )	
		banner = mys.recv(1024) ##only for ssh

		if (len(banner) > 0) :
			add_host_report(HOST_IP, banner)

		destroy_socket(mys)
	except:
		destroy_socket(mys)

def main():
	for I in range(1,255):
		connect_host(I)
	
	print_host_report()	

### main code

if __name__ == '__main__':
	main()
