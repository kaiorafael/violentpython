#!/usr/bin/python
#@kaiux
import socket
from threading import Thread
import optparse

STIMEOUT = 2
OPEN_HOSTS = {}
IP_RANGE = ''

def create_new_socket():
	return socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def destroy_socket(s_object):
	s_object.close()
	del s_object

def add_host_report(ip, banner):
	OPEN_HOSTS[ip] = banner.strip('\n')

def print_host_report():
	print "Found: ", len(OPEN_HOSTS), "hosts"
	for IP, BANNER in OPEN_HOSTS.iteritems():
		print IP, ":", BANNER

def connect_host(HOST_IP):
	try:
		socket.setdefaulttimeout(STIMEOUT)
		mys = create_new_socket()
		mys.connect( (HOST_IP, 22) )	
		banner = mys.recv(1024) ##only for ssh

		if (len(banner) > 0) :
			#add_host_report(HOST_IP, banner)
			print HOST_IP+":"+banner

		destroy_socket(mys)
	except:
		destroy_socket(mys)

def main():
	parser = optparse.OptionParser("%prog -i ip_range: 192.168.1.")
	parser.add_option('-i',dest='ip', type='string', help='gime ip_range')
	(options, args) = parser.parse_args()

	if (options.ip == None):
		print parser.usage
		exit(0)
	else:
		IP_RANGE = options.ip

	for I in range(1,255):
		HOST_IP = IP_RANGE + str(I)

		myT = Thread(target=connect_host,args=(HOST_IP,))
		myT.start()

	#print_host_report()

### main code
if __name__ == '__main__':
	main()
